package templ

type TablesTemplate struct {
	Tables []Table
}

type Table struct {
	Title   string
	Headers []string
	Rows    []Row
}

// Row contains Data which is a slice that contains the data for each column in that row
// This should be ordered to match the order of the Headers
type Row struct {
	Data []string
}
