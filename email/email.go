package email

import (
	"fmt"
	"net/smtp"
)

type Client struct {
	From     string
	Host     string
	Port     string
	password string `json:"-"`
	username string `json:"-"`
}

func New(from, host, port, user, pass string) *Client {
	return &Client{
		From:     from,
		Host:     host,
		Port:     port,
		password: pass,
		username: user,
	}
}

func (c *Client) address() string {
	return c.Host + ":" + c.Port
}

func (c *Client) SendMessage(to []string, msg string) error {
	message := []byte(msg)
	auth := smtp.PlainAuth("", c.username, c.password, c.Host)

	if err := smtp.SendMail(c.address(), auth, c.From, to, message); err != nil {
		return fmt.Errorf("unable to send email: %w", err)
	}

	return nil
}
