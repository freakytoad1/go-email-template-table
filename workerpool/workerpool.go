package workerpool

// WorkerPool is a contract for Worker Pool implementation
type WorkerPool interface {
	Run()
	AddTask(task func())
}

type workerPool struct {
	maxWorker  int
	queuedTask chan func()
}

// New will create an instance of WorkerPool.
func New(maxWorker int) WorkerPool {
	wp := &workerPool{
		maxWorker:  maxWorker,
		queuedTask: make(chan func()),
	}

	return wp
}

func (wp *workerPool) Run() {
	wp.run()
}

func (wp *workerPool) AddTask(task func()) {
	wp.queuedTask <- task
}

func (wp *workerPool) GetTotalQueuedTask() int {
	return len(wp.queuedTask)
}

func (wp *workerPool) run() {
	for i := 0; i < wp.maxWorker; i++ {
		go func() {
			for task := range wp.queuedTask {
				task()
			}
		}()
	}
}
