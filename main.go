package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"sync"

	"github.com/joho/godotenv"
	"gitlab.com/freakytoad1/go-email-template-table/catfact"
	"gitlab.com/freakytoad1/go-email-template-table/email"
	templ "gitlab.com/freakytoad1/go-email-template-table/template"
	"gitlab.com/freakytoad1/go-email-template-table/workerpool"
)

func main() {
	// Got to 'localhost:3000/table' in browser to kick off the handler
	http.HandleFunc("/table", tablePage)
	if err := http.ListenAndServe(":3000", nil); err != nil {
		log.Fatalf("couldn't start server: %v", err)
	}
}

func tablePage(w http.ResponseWriter, r *http.Request) {
	// change these numbers to see different table sizes
	numberOfTables := 2
	numberOfFacts := 5

	// Get all facts required
	kFactsAll := [][]*catfact.Fact{}
	for i := 0; i < numberOfTables; i++ {
		kFacts, err := GetKittyFacts(numberOfFacts)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			break
		}
		kFactsAll = append(kFactsAll, kFacts)
	}

	// Show the facts in browser
	GenerateTemplate(kFactsAll, w)

	// send an email with the facts
	// page is already delivered to browser so print err to console
	if err := SendEmail("kitty facts retrieved!"); err != nil {
		log.Printf("unable to send success email: %v", err)
	}
}

func GenerateTemplate(kFacts [][]*catfact.Fact, w http.ResponseWriter) {
	tables := []templ.Table{}

	for i, factPool := range kFacts {
		// Convert each fact into a row in the table
		table := templ.Table{
			Title:   fmt.Sprintf("Cat Facts %d", i),
			Headers: []string{"fact", "length"},
		}

		// build out the table structure
		for _, fact := range factPool {
			data := templ.Row{
				Data: []string{
					fact.Fact,
					fmt.Sprintf("%d", fact.Length),
				},
			}

			table.Rows = append(table.Rows, data)
		}

		tables = append(tables, table)
	}

	// use our index.html with go template {{ }} call outs for the table data
	tmpl, err := template.ParseFiles("./index.html")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	if err := tmpl.Execute(w, templ.TablesTemplate{Tables: tables}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func SendEmail(msg string) error {
	// load creds from env
	if err := godotenv.Load(".env"); err != nil {
		return err
	}

	em := email.New(os.Getenv("EMAIL_FROM"),
		os.Getenv("EMAIL_HOST"),
		os.Getenv("EMAIL_PORT"),
		os.Getenv("EMAIL_USER"),
		os.Getenv("EMAIL_PASS"),
	)

	if err := em.SendMessage([]string{os.Getenv("EMAIL_TO")}, msg); err != nil {
		return fmt.Errorf("unable to send email message: %w", err)
	}
	return nil
}

func GetKittyFacts(numberOfFacts int) ([]*catfact.Fact, error) {
	kfs := []*catfact.Fact{}

	var mu sync.Mutex

	// Start Worker Pool.
	wp := workerpool.New(5)
	wp.Run()

	var wg sync.WaitGroup

	for i := 0; i < numberOfFacts; i++ {
		wg.Add(1)
		wp.AddTask(func() {
			log.Printf("getting cat fact")
			defer wg.Done()
			kittyFact, err := catfact.New()
			if err != nil {
				log.Printf("could not get cat fact: %v", err)
				return
			}

			mu.Lock()
			defer mu.Unlock()
			kfs = append(kfs, kittyFact)
		})
	}

	wg.Wait()

	if len(kfs) < 1 {
		return nil, fmt.Errorf("no kitty facts returned")
	}

	return kfs, nil
}
