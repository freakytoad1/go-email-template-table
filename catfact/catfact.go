package catfact

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type Fact struct {
	Fact   string `json:"fact"`
	Length int    `json:"length"`
}

func New() (*Fact, error) {
	// request
	resp, err := http.Get("https://catfact.ninja/fact")
	if err != nil {
		return nil, fmt.Errorf("unable to get cat fact: %w", err)
	}

	// unmarshal
	defer resp.Body.Close()
	bodyBytes, _ := ioutil.ReadAll(resp.Body)

	var kittyFact Fact
	if err := json.Unmarshal(bodyBytes, &kittyFact); err != nil {
		return nil, fmt.Errorf("unable to unmarshal cat fact: %w", err)
	}

	return &kittyFact, nil
}
