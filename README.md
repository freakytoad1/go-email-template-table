# Go Email Template Table

Simple way to display a dynamically sized html table from a Go Struct using Go Templates.

This project features:
- Worker Pools
- Http Requests
- Http API Server
- Go Templates
- Sending an email

## How to run

1. `go run main.go`
1. Navigate to `localhost:3000/table` in browser
1. See the kitty facts appear!

To get the email going, create a `.env` file from the example and fill out the details. The email sends after the html page is displayed. If an error occurs it will print to the console.